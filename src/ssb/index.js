const { promisify: p } = require('util')

// const { PubSub } = require('apollo-server')
// const pubsub = new PubSub()

const PostUploadFile = require('./mutations/post-upload-file')
// const StreamPeers = require('./subscriptions/stream-peers')

module.exports = function (ssb) {
  if (!ssb) throw new Error('invalid ssb')
  if (!ssb.blobs && !ssb.hyperBlobs) throw new Error('requires either ssb-blobs OR ssb-hyper-blobs')

  const postUploadFile = PostUploadFile(ssb)
  // const streamPeers = StreamPeers(ssb, pubsub)

  return {
    postUploadFile: p(postUploadFile)
    // streamPeers: p(streamPeers)
  }
}
