const pull = require('pull-stream')
const toPull = require('stream-to-pull-stream')
const pullBoxStream = require('pull-box-stream')
const crypto = require('crypto')

const zeros = Buffer.alloc(24, 0)

module.exports = function PostUploadFile (ssb) {
  let max = ssb.config.blobs && ssb.config.blobs.max
  if (typeof max !== 'number') max = 5 * 1024 * 1024 // default: 5MB

  const processing = new Set()
  // HACK
  // we were seeing multiple triggers of this resolver for a single fileUpload graphql query
  // not yet sure why. for the moment we are tracking internally here what we are processing
  // and skipping if it's a duplicate

  return function postUploadFile (file, size, encrypt, cb) {
    file
      .then(file => {
        if (processing.has(file.name)) return
        // note sure if should callback with anything
        // worried about triggering callback multiple times inside promisified function.

        processing.add(file.name)

        let processor
        if (size < max && ssb.blobs) {
          processor = encrypt ? ssbBlobsEncrypt : ssbBlobsPlain
        } else if (ssb.hyperBlobs) {
          processor = ssbHyperBlobs
        }
        if (!processor) {
          return cb(new Error('could not match file upload to a store, make sure ssb-blobs / ssb-hyper-blobs installed'))
        }

        processor(ssb, file, (err, data) => {
          processing.delete(file.name)
          cb(err, data)
        })
      })
      .catch(cb)
  }
}

function ssbHyperBlobs (ssb, file, cb) {
  const { mimetype: mimeType, createReadStream } = file
  let size = 0

  return pull(
    toPull.source(createReadStream()),
    pull.through(chunk => {
      size += (chunk.byteLength || chunk.length)
    }),
    ssb.hyperBlobs.add((err, data) => {
      if (err) return cb(err)

      // NOTE we convert to base64 for addresses/keys to be persisted in ssb

      cb(null, {
        type: 'hyper',
        mimeType,
        driveAddress: hexToBase64(data.driveAddress),
        blobId: data.blobId,
        readKey: hexToBase64(data.readKey),
        size
      })
    })
  )
}
function hexToBase64 (str) {
  return Buffer.from(str, 'hex').toString('base64')
}

function ssbBlobsEncrypt (ssb, { mimetype: mimeType, createReadStream }, cb) {
  // Adapted from: https://github.com/ssbc/ssb-blob-files/blob/d35fcfa5f580aeb7b921ac72e9b4ad9818a83d6d/async/publish-blob.js#L49-L80
  pull(
    toPull.source(createReadStream()),
    createHashSink(function (err, outerBuffers, key) {
      if (err) return cb(err)

      pull(
        pull.values(outerBuffers),
        pullBoxStream.createBoxStream(key, zeros),
        createHashSink(function (err, innerBuffers, hash) {
          if (err) return cb(err)
          const blobId = '&' + hash.toString('base64') + '.sha256'

          pull(
            pull.values(innerBuffers),
            ssb.blobs.add(blobId, function (err) {
              if (err) return cb(err)

              ssb.blobs.push(blobId, function (err) {
                if (err) return cb(err)

                const unbox = key.toString('base64') + '.boxs'

                cb(null, {
                  type: 'ssb',
                  blobId,
                  unbox,
                  size: innerBuffers.join('').length,
                  mimeType
                })
              })
            })
          )
        })
      )
    })
  )
}

function createHashSink (cb) {
  const hash = crypto.createHash('sha256')
  const buffers = []

  return pull.drain(
    (data) => {
      data = typeof data === 'string' ? Buffer.from(data) : data
      buffers.push(data)
      hash.update(data)
    },
    (err) => cb(err, buffers, hash.digest())
  )
}

function ssbBlobsPlain (ssb, { mimetype: mimeType, createReadStream }, cb) {
  let size = 0
  pull(
    toPull.source(createReadStream()),
    pull.through(chunk => { size += chunk.byteLength }),
    ssb.blobs.add((err, blobId) => {
      if (err) return cb(err)
      cb(null, {
        type: 'ssb',
        blobId,
        size,
        mimeType
      })
    })
  )
}
