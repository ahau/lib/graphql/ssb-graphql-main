const { promisify } = require('util')
const PERSONAL_TYPE = '__personal__'

/* eslint-disable brace-style */
module.exports = function LoadPersonalGroup (ssb) {
  return function loadPersonalGroup (cb) {
    if (cb === undefined) return promisify(loadPersonalGroup)()

    ssb.tribes.findByFeedId(ssb.id, (err, groups) => {
      if (err) return cb(err)

      const personalGroups = groups.filter(g => g.states[0].state.name === PERSONAL_TYPE)

      if (personalGroups.length) {
        if (personalGroups.length > 1) console.warn('ssb-graphql/main found multiple personal groups, using first one')
        cb(null, personalGroups[0].groupId)
      }
      else {
        ssb.tribes.create({}, (err, data) => {
          if (err) return cb(err)

          ssb.tribes.link.create({ group: data.groupId, name: PERSONAL_TYPE }, (err, link) => {
            if (err) return cb(err)

            cb(null, data.groupId)
          })
        })
      }
    })
  }
}
