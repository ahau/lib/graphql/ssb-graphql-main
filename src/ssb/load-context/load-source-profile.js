const { promisify } = require('util')
const clarify = require('clarify-error')

/* eslint-disable brace-style */
module.exports = function LoadSourceProfile (ssb) {
  return function loadSourceProfile (profiles, personalGroupId, cb) {
    if (cb === undefined) return promisify(loadSourceProfile)(personalGroupId)

    const personalProfiles = filterProfiles(profiles, 'person/source', personalGroupId)

    if (personalProfiles.length) {
      if (personalProfiles.length > 1) console.warn('ssb-graphql/main found multiple personal profiles, using first one')
      cb(null, personalProfiles[0].key)
    }
    else createSourceProfile(personalGroupId, cb)
  }

  function createSourceProfile (personalGroupId, cb) {
    /* create a new profile */
    const details = {
      authors: {
        add: [ssb.id]
      },
      recps: [personalGroupId]
    }

    getOldProfile((err, legacyProfile) => {
      if (err) console.log("couldn't find a legacy profile")
      if (legacyProfile) copyDetails(details, legacyProfile)

      delete details.source
      // NOTE source profiles do not support the "source" field
      ssb.profile.person.source.create(details, (err, profileId) => {
        if (err) return cb(Error('failed to create source profile', { cause: err }))

        ssb.profile.link.create(profileId, {}, (err, link) => {
          if (err) return cb(clarify(err, 'failed to create link to source profile'))

          cb(null, profileId)

          // tidyup!
          if (legacyProfile) {
            ssb.profile.person.group.tombstone(legacyProfile.key, { reason: 'automated upgrade' }, (err) => {
              if (err) console.log('trouble tombstoning old profile', err)
            })
          }
        })
      })
    })

    function getOldProfile (cb) {
      /* check for LEGACY personal profile to copy from */
      ssb.profile.findByFeedId(ssb.id, (err, profiles) => {
        if (err) return cb(clarify(err, 'failed to findByFeedId'))

        const legacyProfile = profiles.private.find(profile => (
          (profile.type === 'person' || profile.type === 'profile/person') &&
          profile.recps.length === 1 &&
          profile.recps[0] === personalGroupId
        ))

        if (legacyProfile) cb(null, legacyProfile)
        else cb(null)
      })
    }
  }
}

function filterProfiles (profiles, type, groupId) {
  return profiles.filter(p => (
    type === p.type &&
    p.recps.length === 1 &&
    p.recps[0] === groupId
  ))
}

function copyDetails (target, source) {
  const ignoreFields = new Set(['key', 'type', 'authors', 'customFields', 'originalAuthor', 'conflictFields', 'states'])
  const simpleSetFields = new Set(['altNames', 'school', 'education']) // get presents these as Arrays

  for (const [key, value] of Object.entries(source)) {
    if (ignoreFields.has(key)) continue

    if (simpleSetFields.has(key)) {
      if (value.length) target[key] = { add: value }
      continue
    }

    target[key] = value
  }
}
