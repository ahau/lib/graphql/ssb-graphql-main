const { promisify } = require('util')
const clarify = require('clarify-error')

const PATAKA = 'pataka'
const PERSON = 'person'

// NOTE duplicate from @ssb-graphql/profile bulk update

module.exports = function LoadContext (ssb, opts = {}) {
  const loadPublicProfile = require('./load-public-profile')(ssb)
  const loadPersonalGroup = require('./load-personal-group')(ssb)
  const loadSourceProfile = require('./load-source-profile')(ssb)
  const loadPersonalSettings = require('./load-personal-settings')(ssb)

  const {
    type = PERSON
  } = opts

  const context = {
    // public: {
    //   feedId,
    //   profileId
    // },
    // personal: {
    //   groupId,
    //   profileId,
    //   settingsId
    // }
  }

  return function loadContext (cb) {
    if (cb === undefined) return promisify(loadContext)()

    switch (type) {
      case PERSON: return loadPersonContext(cb)
      case PATAKA: return loadPatakaContext(cb)
    }
  }

  function loadPersonContext (cb) {
    if (!ssb.profile) return cb(PluginError('ssb-profile'))
    if (!ssb.settings) return cb(PluginError('ssb-settings'))
    if (!ssb.recpsGuard) return cb(PluginError('ssb-recps-guard'))

    loadPersonalGroup((err, personalGroupId) => {
      if (err) return cb(clarify(err, 'failed to loadPersonalGroup'))
      ssb.profile.findByFeedId(ssb.id, (err, profiles) => {
        if (err) return cb(clarify(err, 'failed to findByFeedId'))

        loadPublicProfile(profiles.public, PERSON, (err, profileId) => {
          if (err) return cb(clarify(err, 'failed to loadPublicProfile'))
          context.public = {
            feedId: ssb.id,
            profileId
          }

          if (!ssb.tribes) {
            console.log('ssb-tribes not installed, @ssb-graphql/main add personal/ group context')
            delete context.personal
            return cb(null, context)
          }
          loadSourceProfile(profiles.private, personalGroupId, (err, profileId) => {
            if (err) return cb(Error('failed to loadSourceProfile', { cause: err }))
            loadPersonalSettings(personalGroupId, (err, settingsId) => {
              if (err) return cb(clarify(err, 'failed to loadPersonalSettings'))

              context.personal = {
                groupId: personalGroupId,
                profileId,
                settingsId
              }

              cb(null, context)
            })
          })
        })
      })
    })
  }

  function loadPatakaContext (cb) {
    if (!ssb.profile) return cb(PluginError('ssb-profile'))
    if (!ssb.settings) return cb(PluginError('ssb-settings'))
    if (!ssb.recpsGuard) return cb(PluginError('ssb-recps-guard'))

    ssb.profile.findByFeedId(ssb.id, (err, profiles) => {
      if (err) return cb(err)
      loadPublicProfile(profiles.public, PATAKA, (err, profileId) => {
        if (err) return cb(err)

        context.public = {
          feedId: ssb.id,
          profileId
        }
        cb(null, context)
      })
    })
  }
}

function PluginError (name) {
  return new Error(`load-context currently expects ${name} to be installed`)
}
