const { GraphQLScalarType, GraphQLError } = require('graphql')
const { promisify: p } = require('util')

const { Date: GraphqlDate, EdtfDate, EdtfInterval } = require('graphql-edtf')
const toScuttlebuttURI = require('ssb-serve-blobs/id-to-url')
const toHyperURI = require('ssb-hyper-blobs/data-to-url')
const LinkedProfileIds = require('./ssb/queries/get-linked-profile-ids')

module.exports = function Resolvers (ssb) {
  const { port: ssbPort, hostname: ssbHostname } = ssb.config?.serveBlobs || {}
  const { port: hyperPort } = ssb.config?.hyperBlobs || {}

  const { postUploadFile } = require('./ssb')(ssb)

  const _setAutoPrune = (value, cb) => {
    ssb.hyperBlobs.autoPrune.set(value, (err) => {
      // false means autoPrune.set failed
      if (err) return cb(null, false)

      // true means autoPrune.set succeeded
      cb(null, true)
    })
  }

  const _getAutoPrune = (cb) => {
    ssb.hyperBlobs.autoPrune.get((err, config) => {
      if (err) return cb(err)

      return cb(null, config)
    })
  }

  const setAutoPrune = p(_setAutoPrune)
  const getAutoPrune = p(_getAutoPrune)
  const getLinkedProfiles = p(LinkedProfileIds(ssb))

  return {
    Query: {
      whoami: (_, __, context) => context,
      hyperBlobsAutoPruneConfig: () => getAutoPrune()
    },
    CurrentIdentity: {
      linkedProfileIds: () => getLinkedProfiles()
    },
    Blob: {
      __resolveType (blob, context, info) {
        switch (blob.type) {
          case 'ssb': return 'BlobScuttlebutt'
          case 'hyper': return 'BlobHyper'
          default: return 'BlobScuttlebutt' // the og blob
        }
      }
    },
    BlobScuttlebutt: {
      uri (blob) {
        const unbox = blob.unbox && blob.unbox.replace('.boxs', '')
        return toScuttlebuttURI(blob.blobId, { port: ssbPort, hostname: ssbHostname, unbox })
      }
    },
    BlobHyper: {
      uri (blob) {
        return toHyperURI(blob, { port: hyperPort })
        // NOTE this doesn't yet support hostname, but that's not yet needed
        // as hyperblobs are for private artefacts currently
      }
    },
    Tombstone: {
      date: ({ date }) => {
        if (typeof date !== 'number') return date
        return new Date(date)
      }
    },
    Mutation: {
      uploadFile: (_, { file, size, encrypt = false }) => postUploadFile(file, size, encrypt),
      saveHyperBlobsAutoPruneConfig: async (_, { disable, startDelay, maxRemoteSize, intervalTime }) => {
        const newConfig = (disable)
          ? false
          : { startDelay, maxRemoteSize, intervalTime }
        return setAutoPrune(newConfig)
      }
    },
    Upload: new GraphQLScalarType({
      name: 'Upload',
      description: 'The `Upload` scalar type represents a file upload.',
      parseValue (value) {
        // WARNING! This hack has been put in place to temporarily fix a bug with file uploading
        // it seems that the value passed into here using:
        // - apollo-upload-client
        // - graphql-express
        // - graphqlUploadExpress
        // - graphql-upload
        // doesnt pass the check "value instanceof Upload"
        // this is only a temporary hack and will bipass parsing the input

        return value.promise

        // if (value instanceof Upload) return value.promise
        // throw new GraphQLError('Upload value invalid.')
      },
      parseLiteral (ast) {
        throw new GraphQLError('Upload literal unsupported.', ast)
      },
      serialize () {
        throw new GraphQLError('Upload serialization unsupported.')
      }
    }),
    Date: GraphqlDate,
    EdtfDate,
    EdtfInterval
  }
}
