const gql = require('graphql-tag')

module.exports = gql`
  scalar Date
  scalar EdtfDate
  scalar EdtfInterval
  scalar Upload

  """
  Input for a tombstone of an entity. E.g. profile, story or whakapapa.
  """
  input TombstoneInput {
    date: Date
    reason: String
  }

  """
  Date of death for an entity. E.g. profile, story, artefact or whakapapa.
  """
  type Tombstone {
    date: Date
    reason: String
  }

  """
  Secure Scuttlebutt current identity
  """
  type CurrentIdentity @key(fields: "id") {
    public: PublicIdentity
    personal: PersonalIdentity
    linkedProfileIds: [ID]
  }

  type PublicIdentity {
    feedId: ID!
    profileId: ID
  }
  type PersonalIdentity {
    groupId: ID!
    profileId: ID
    settingsId: ID
  }

  enum BlobType {
    ssb
    hyper
  }

  """
  binary file storage
  """
  interface Blob {
    type: BlobType!
    mimeType: String!
    size: Int!
    blobId: ID!
    uri: String
  }

  """
  Blobs of the ssb-blobs variety
  """
  type BlobScuttlebutt implements Blob {
    type: BlobType!
    mimeType: String!
    size: Int!
    blobId: ID!
    uri: String

    unbox: String!
  }

  """
  Blobs of the ssb-hyper-blobs variety
  """
  type BlobHyper implements Blob {
    type: BlobType!
    mimeType: String!
    size: Int!
    blobId: ID!
    uri: String

    driveAddress: String!
    readKey: String!
  }

  """
  Input for ssb and hpyer type blobs. used by e.g @ssb-graphql/artefact
  """
  input BlobInput {
    type: BlobType!
    blobId: ID!
    mimeType: String!
    size: Int!

    unbox: String
    driveAddress: String
    readKey: String
  }

  type HyperBlobsAutoPruneConfig {
    maxRemoteSize: Float!
    intervalTime: Float!
    startDelay: Float!
  }

  """
  Secure Scuttlebutt replication peer
  """
  type Peer @key(fields: "id") {
    id: ID
    state: String
  }

  type Query {
    """
    Returns information about the default Person associated with this SSB identity
    """
    whoami: CurrentIdentity

    """
    Returns the current state of config.hyperBlobs.autoPrune
    """
    hyperBlobsAutoPruneConfig: HyperBlobsAutoPruneConfig
  }

  type Mutation {
    """
    Upload a file to the Secure Scuttlebutt network (using ssb-blobs)
    """
    uploadFile(file: Upload!, size: Int!, encrypt: Boolean): Blob

    saveHyperBlobsAutoPruneConfig(disable: Boolean, startDelay: Float, maxRemoteSize: Float, intervalTime: Float): Boolean
  }

  """
  Return information about Secure Scuttlebutt replication peers
  """
  type Subscription {
    peers: [Peer]
  }
`
