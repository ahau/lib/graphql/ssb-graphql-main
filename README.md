# GraphQL for Secure Scuttlebutt

Main GraphQL types and resolvers for Secure Scuttlebutt.
It provides primitives for CurrentIdentity, file-upload, Date handling etc.

## Usage

`npm i -S @ssb-graphql/main`

## Example Usage

```js
const { ApolloServer } = require('apollo-server-express')
const { buildFederatedSchema } = require('@apollo/federation');

const Server = require('ssb-server')
const Config = require('ssb-config/inject')

const config = Config({})

const ssb = Server
  .use(require('ssb-blobs')) // << required
  .use(require('ssb-serve-blobs')) // << required
  .call(null, config)

const main = require('@ssb-graphql/main')(ssb)

main.loadContext((err, context) => {
  if (err) throw err

  const server = new ApolloServer({
    schema: buildFederatedSchema([
      { typeDefs: main.typeDefs, resolvers: main.resolvers },
      // add other types + resolvers here!
    ]),
    context
  })
})
```

## API

### `require('@ssb-graphql/main')(ssb, opts) => main`

where:
- `ssb` is a scuttlebutt instance
- `opts` *Object* (optional) is of form `{ type: String }` where:
    - `type` is the type of profile created (default: `'person'`)
    

### `main.loadContext(cb)`

Where `cb` is a callback of signature `cb(err, context)` and `context` has form

```js
{
  public: {
    feedId,
    profileId  // requires ssb-profile
  },
  personal: {
    groupId,   // requires ssb-tribes
    profileId  // requires ssb-profile + ssb-tribes
  }
}
```

where:
- the `public` scope are things meant for the public domain,
  - `public.feedId` is the public id of your scuttlebutt instance (this is not a profileId)
  - `public.profileId` points to an unecrypted totally public profile
- the `personal` scope is for things only you will use
  - `personal.groupId` is the id of your personal group (for encrypted notes to self)
  - `personal.profileId` points to a profile for you which encrypted to your personal group (so no-one else will see these details)

NOTE:
- **this will create new records** (profiles / private group) and link them to your feed if they do not already exist
- creating profiles requires `ssb-profile` installed
- creating a personal group with a profile in it further requires `ssb-tribes`
    - if `ssb-tribes` is not installed, this will be skipped
    - if `type` was set to `pataka` when `main` was instantiated, this will also be skipped



## Requirements

An `ssb-server` with the following plugins:
- `ssb-blobs`
- `ssb-serve-blobs`

For `main.loadContext`:
- `ssb-profile`
- `ssb-tribes` (optional, will skip functions if not present)

## Config

Scuttlebutt config that this module listens to:

```js
{
  blobs: {
    max: Number
  },
  serveBlobs: {
    port: Number,
    hostname: String
  },
  hyperBlobs: {
    port: Number
  }
}
```

## API

See `/src/typeDefs.js` for the most up to date details on what's offered by this module.

## TODO

run `npm test` to run tests

