const tape = require('tape')
const { promisify: p } = require('util')
const LegacyProfileCrut = require('ssb-profile/legacy')

const { SSB } = require('../test-bot')
const LoadContext = require('../../src/ssb/load-context')
const LoadPersonalGroup = require('../../src/ssb/load-context/load-personal-group')

tape('load-context (LEGACY: leaky public profile)', async (t) => {
  // context: public profiles used have the same fields as private profiles (except no recps)
  // In out code, we accidentally bulk updated public profiles with phone numbers which should have been private
  // This has since been closed, and the network was forked/ restarted to resolve the problem
  // This test is no longer really needed but is nice proof that even if phone number was published in an update,
  // our crut.get will not display that info
  const ssb = SSB()
  const loadContext = LoadContext(ssb)

  const context = await loadContext()

  const id = context.public.profileId
  const content = {
    type: 'profile/person',
    phone: { set: '3030303030' }, // should be private
    tangles: {
      profile: { root: id, previous: [id] }
    }
  }
  await p(ssb.db.create)({ content, allowPublic: true })
  t.pass('publish a "bad" update leaking private data to public profile')

  const _context = await loadContext()
  t.true(_context, 'run loadContext successfully')

  const legacyCrut = LegacyProfileCrut(ssb)
  const profile = await p(legacyCrut.get)(id)
  t.deepEqual(profile.phone, undefined, 'private field "phone" is just ignored')

  ssb.close()
  t.end()
})

tape('load-context (LEGACY: personal profile type changed)', async (t) => {
  // context: the default for profiles in groups was updated to be called "profile.person.group"
  // It has the same type, but now exlcudes contact details
  // Unfortunately your personal profiles used this type as well, but we want contact details on that one!
  // The solution is to migrate older personal profiles that are found over to the new type `profile/person/source`
  //
  // NOTE: this migration drops contact details, but this was deemed acceptable

  const ssb = SSB()
  const loadContext = LoadContext(ssb)
  const loadPersonalGroup = LoadPersonalGroup(ssb)

  // /* make an old stle personal profile */
  const groupId = await loadPersonalGroup()
  const details = {
    preferredName: 'manu',
    authors: { add: [ssb.id] },
    recps: [groupId]
  }
  // ssb.post(m => ssb.get({ id: m.key, private: true }, (err, value) => console.log(value.content)))
  const oldProfileId = await p(ssb.profile.person.group.create)(details)

  await p(ssb.profile.link.create)(oldProfileId)

  const context = await loadContext()
    .catch(err => {
      console.error('failed to loadContext', err)
      t.error(err, 'failed to loadContext')
    })
  t.notEqual(context.personal.profileId, oldProfileId, 'a new personal profile has been made')

  const newProfile = await p(ssb.profile.person.source.get)(context.personal.profileId)
  for (const [key, value] of Object.entries(newProfile)) {
    if (key === 'preferredName') {
      t.equal(value, 'manu', 'fields copied over')
      continue
    }

    /* fields we expect to have things */
    if (key === 'key') continue
    if (key === 'type') continue
    if (key === 'authors') continue

    /* rest should be "empty" */
    if (value === null) continue
    if (Array.isArray(value) && value.length === 0) continue
  }

  const oldProfile = await p(ssb.profile.person.group.get)(oldProfileId)
  t.notEqual(oldProfile.tombstone, null, 'the original profile is tombstoned')

  ssb.close()
  t.end()
})
