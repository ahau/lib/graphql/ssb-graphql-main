const tape = require('tape')
const { promisify: p } = require('util')
const { generate } = require('ssb-keys')
const { isFeed, isCloakedMsgId: isGroup } = require('ssb-ref')

const TestBot = require('../test-bot')
const { SSB } = TestBot
const LoadContext = require('../../src/ssb/load-context')

tape('load-context (default full install)', async (t) => {
  const name = 'load-context-' + Date.now()
  const keys = generate()

  let ssb = SSB({ name, keys, startUnclean: true })
  let loadContext = LoadContext(ssb)

  const context = await loadContext()

  /* check { public } */
  t.true(isFeed(context.public.feedId), 'has feedId')

  let profile = await p(ssb.profile.person.public.get)(context.public.profileId)
  t.deepEqual(profile.recps, null, 'public profile (no recps)')
  t.deepEqual(profile.type, 'person', 'is profile/person')

  /* check { personal } */
  t.true(isGroup(context.personal.groupId), 'has personal.groupId')

  profile = await p(ssb.profile.person.source.get)(context.personal.profileId)
  t.deepEqual(profile.recps, [context.personal.groupId], 'source profile is encrypted to personal group')
  t.deepEqual(profile.type, 'person/source', 'is profile/person/source')

  const settings = await p(ssb.settings.get)(context.personal.settingsId)
  t.deepEqual(settings.recps, [context.personal.groupId], 'settings encrypted to feedId')
  t.deepEqual(settings.type, 'settings', 'is settings')

  // reload the context to see if the same stuff comes back
  const reloadedContext = await loadContext()
  t.deepEqual(reloadedContext, context, 'loading context again returns previous values')

  /* check persistance */
  await p(ssb.close)()
  ssb = SSB({ name, keys, startUnclean: true })
  loadContext = LoadContext(ssb)

  const persistedContext = await loadContext()
  t.deepEqual(context, persistedContext, 'context persisted!')

  ssb.close()
  t.end()
})

tape('load-context (without ssb-recps-guard', async (t) => {
  try {
    await TestBot({ recpsGuard: false })
  } catch (err) {
    t.match(err.message, /expects.* ssb-recps-guard/)
    t.end()
  }
})
