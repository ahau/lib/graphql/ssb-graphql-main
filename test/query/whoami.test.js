const tape = require('tape')
const { promisify: p } = require('util')

const TestBot = require('../test-bot')
const { isFeed, isMsg, isCloakedMsgId: isGroup } = require('ssb-ref')

const { InitGroup } = require('../helpers/')

tape('whoami', async (t) => {
  const { ssb, apollo } = await TestBot({ loadContext: true })
  t.plan(5)

  const result = await apollo.query({
    query: `{
      whoami { 
        public {
          feedId
          profileId
        }
        personal {
          groupId
          profileId
        }
      } 
    } `
  })

  // Ensure that basic query worked.
  t.error(result.errors, 'query should not return errors')

  const { feedId, profileId } = result.data.whoami.public
  t.true(isFeed(feedId), 'has feedId')
  t.true(isMsg(profileId), 'public profile (id)')

  const { groupId, profileId: _profileId } = result.data.whoami.personal
  t.true(isGroup(groupId), 'has feedId')
  t.true(isMsg(_profileId), 'personal profile (id)')

  ssb.close()
})

tape('whoami (linkedProfileIds)', async (t) => {
  const { ssb, apollo } = await TestBot({ loadContext: true })
  const initGroup = InitGroup(ssb)

  t.plan(6)

  async function getWhoami () {
    const res = await apollo.query({
      query: `{
        whoami { 
          public {
            feedId
            profileId
          }
          personal {
            groupId
            profileId
          }
          linkedProfileIds
        } 
      } `
    })

    t.error(res.errors, 'query whoami without errors')

    return res.data.whoami
  }

  let whoami = await getWhoami()

  t.deepEquals(
    whoami.linkedProfileIds,
    [whoami.public.profileId, whoami.personal.profileId].sort(),
    'linkedProfileIds returns your personal and public profiles'
  )

  // create a tribe
  const { community, person } = await initGroup({ preferredName: 'My family' })

  // recall the whoami query
  whoami = await getWhoami()

  t.deepEquals(
    whoami.linkedProfileIds,
    [whoami.public.profileId, whoami.personal.profileId, person.groupProfileId, person.adminProfileId].sort(),
    'linkedProfileIds returns your linked profiles in a group'
  )

  const tombstone = {
    date: Date.now(),
    reason: 'deleted'
  }
  // tombstone the community
  await p(ssb.profile.community.group.tombstone)(community.groupProfileId, { tombstone })
  await p(ssb.profile.community.public.tombstone)(community.publicProfileId, { tombstone, allowPublic: true })

  // recall the whoami query once again
  whoami = await getWhoami()

  t.deepEquals(
    whoami.linkedProfileIds,
    [whoami.public.profileId, whoami.personal.profileId].sort(),
    'the profiles in the group werent returned in linkedProfileIds when the group was tomsbtoned'
  )
  ssb.close()
})

tape('whoami (pataka, no-tribe)', async (t) => {
  const { ssb, apollo } = await TestBot({
    loadContext: true,
    installTribes: false,
    profileType: 'pataka'
  })

  const result = await apollo.query({
    query: `{
      whoami { 
        public {
          feedId
          profileId
        }
        personal {
          groupId
          profileId
        }
      } 
    } `
  })

  // Ensure that basic query worked.
  t.error(result.errors, 'query should not return errors')

  const { feedId, profileId } = result.data.whoami.public
  t.true(isFeed(feedId), 'has feedId')
  t.true(isMsg(profileId), 'public profile (id)')

  t.equal(result.data.whoami.personal, null, 'no personal data')

  ssb.close(t.end)
})
