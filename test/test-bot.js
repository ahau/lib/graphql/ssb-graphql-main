import AhauClient from 'ahau-graphql-client'

const ahauServer = require('ahau-graphql-server')
const Server = require('scuttle-testbot')
const fetch = require('node-fetch')

const Main = require('../')

async function TestBot (opts = {}) {
  // opts = {
  //   name: String,
  //   keys: SecretKeys,
  //   startUnclean: Boolean,
  //
  //   profileType
  // }

  const ssb = SSB(opts)

  const apollo = await Apollo(ssb, {
    port: opts.graphql && opts.graphql.port,
    addSchema: opts.profile
      ? [require('@ssb-graphql/profile')(ssb)]
      : [],
    loadContext: opts.loadContext,
    profileType: opts.profileType
  })

  return {
    ssb,
    apollo
  }
}

function SSB (opts = {}) {
  opts.serveBlobs = opts.serveBlobs || {}
  opts.serveBlobs.port = opts.serveBlobs.port || (6000 + Math.floor(Math.random() * 2e3))

  const stack = Server // eslint-disable-line
    .use(require('ssb-db2/core'))
    .use(require('ssb-classic'))
    .use(require('ssb-db2/compat/db'))
    .use(require('ssb-db2/compat/feedstate'))
    .use(require('ssb-db2/compat/log-stream'))
    .use(require('ssb-profile'))
    .use(require('ssb-settings'))

  if (opts.installTribes !== false) {
    stack.use(require('ssb-box2'))
    stack.use(require('ssb-tribes'))
  }

  if (opts.installBlobs !== false) {
    stack.use(require('ssb-blobs'))
    stack.use(require('ssb-serve-blobs'))
  }

  if (opts.installHyperBlobs) {
    stack.use(require('ssb-hyper-blobs'))
    opts.hyperBlobs = opts.hyperBlobs || {}
    opts.hyperBlobs.port = opts.hyperBlobs.port || (8000 + Math.floor(Math.random() * 2e3))
  }

  // has to be added last for its hooks
  if (opts.recpsGuard !== false) {
    stack.use(require('ssb-recps-guard'))
  }

  return stack({
    ...opts,
    noDefaultUse: true,
    box2: {
      ...opts.box2,
      legacyMode: true
    }
  })
}

async function Apollo (ssb, opts = {}) {
  const {
    port = 10e3 + Math.floor(Math.random() * 2e3),
    addSchema = [],
    loadContext = true,
    profileType
  } = opts

  let main
  try {
    main = Main(ssb, { type: profileType })
  } catch (err) {
    ssb.close()
    throw err
  }

  let context = {}
  if (loadContext) {
    context = await main.loadContext()
      .catch(err => {
        ssb.close()
        throw err
      })
  }
  const schemas = [main, ...addSchema]

  /* start server */
  const httpServer = await ahauServer({ context, schemas, port })
  ssb.close.hook((close, args) => {
    httpServer.close()
    close(...args)
  })

  return new AhauClient(port, { fetch, isTesting: true })
}

TestBot.SSB = SSB
TestBot.Apollo = Apollo

module.exports = TestBot
